# Listen.

### What is Listen?

Listen is an iOS application that synthesizes speech into text using Microsoft Azure's cognitive services. The user can click on Listen and the device will Listen for voice input and display the sound on screen in text format.

### Who is Listen for?

Listen is for people who are hard of hearing and need a way to be able to communicate with those around them. It can be anyone from someone who was born deaf, so someone who has temporarily been rendered deaf due to illness.

### Getting Started

* Clone the repository to your local machine.
* Open the repository in Xcode. Make sure you open the helloworld.xcworkspace file.
* If your device asks you to update some Xcode packages, make sure that you update them and restart Xcode.
* Once you have the project open, change the device to an iPhone 8 before running the project.
* Click the play button in the top left hand corner to run the project.

### Tech

Listen was created using:

* [Swift] - For the front-end and business logic of the application
* [Microsoft Azure's Cognitive Services] - for the artificial intelligence part of the application


   [Swift]: <https://swift.org/documentation/>
   [Microsoft Azure's Cognitive Services]: <https://azure.microsoft.com/en-us/services/cognitive-services/>


![alt Listen](images/listen.png)