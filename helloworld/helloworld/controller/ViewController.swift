import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var resultLabel: UILabel!
    
    @IBOutlet weak var listenButton: UIButton!
    
    @IBOutlet weak var clearButton: UIButton!
    
    var sub: String!
    var region: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // load subscription information
        sub = "f04e39ac01ac44abba43870e5a986257"
        region = "uksouth"
        
        //some programatic styling
        resultLabel.lineBreakMode = .byWordWrapping
        resultLabel.numberOfLines = 0
        
        listenButton.layer.cornerRadius = 20
        
        clearButton.backgroundColor = .clear
        clearButton.layer.cornerRadius = 20
        clearButton.layer.borderWidth = 1
        clearButton.layer.borderColor = UIColor(red:1.00, green:0.73, blue:0.59, alpha:1.0).cgColor
        
        listenButton.addTarget(self, action:#selector(self.listenButtonClicked), for: .touchUpInside)
        
        self.view.addSubview(resultLabel)
        self.view.addSubview(listenButton)
    }
    
    //clears input field
    @IBAction func clearButtonTap(_ sender: UIButton) {
        resultLabel.text = ""
    }
    
    //run function to start listening on click
    @objc func listenButtonClicked() {
        DispatchQueue.global(qos: .userInitiated).async {
            self.recognizeFromMic()
        }
    }
    
    //synthesize speech to text
    func recognizeFromMic() {
        var speechConfig: SPXSpeechConfiguration?
        do {
            try speechConfig = SPXSpeechConfiguration(subscription: sub, region: region)
        } catch {
            print("error \(error) happened")
            speechConfig = nil
        }
        
        speechConfig?.speechRecognitionLanguage = "en-US"
        
        let audioConfig = SPXAudioConfiguration()
        
        let reco = try! SPXSpeechRecognizer(speechConfiguration: speechConfig!, audioConfiguration: audioConfig)
        
        reco.addRecognizingEventHandler() {reco, evt in
            print("intermediate recognition result: \(evt.result.text ?? "(no result)")")
            self.updateLabel(text: evt.result.text, color: .gray)
        }
        
        updateLabel(text: "Listening...", color: UIColor(red:1.00, green:0.73, blue:0.59, alpha:1.0))
        print("Listening...")
        
        let result = try! reco.recognizeOnce()
        print("recognition result: \(result.text ?? "(no result)")")
        updateLabel(text: result.text, color: .white)
    }
    
    //update label
    func updateLabel(text: String?, color: UIColor) {
        DispatchQueue.main.async {
            if (text!.isEmpty) {
                self.resultLabel.text = "Sorry, I didn't hear you. Could you repeat that?"
                self.resultLabel.textColor = UIColor(red:1.00, green:0.73, blue:0.59, alpha:1.0)
            } else {
                self.resultLabel.text = text
                self.resultLabel.textColor = color
            }
            
        }
    }
}
